from fastapi import APIRouter, Depends, Response
from typing import List, Union
from queries.vacations import (
    VacationIn,
    VacationOut,
    VacationRepository,
    Error,
)

router = APIRouter()


@router.post(
    "/vacations",
    response_model=Union[VacationOut, Error],
)
def create_vacation(
    vacation: VacationIn,
    response: Response,
    repo: VacationRepository = Depends(),
):
    # response.status_code = 400
    return repo.create(vacation)


@router.get("/vacations", response_model=Union[Error, List[VacationOut]])
def get_all(
    repo: VacationRepository = Depends(),
):
    return repo.get_all()


@router.put("/vacations/{vacation_id}", response_model=Union[Error, VacationOut])
def update_vacation(
    vacation_id: int,
    vacation: VacationIn,
    repo: VacationRepository = Depends(),
) -> Union[Error, VacationOut]:
    return repo.update(vacation_id, vacation)
